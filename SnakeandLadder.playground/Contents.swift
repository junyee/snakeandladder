//: Playground - noun: a place where people can play
//

import UIKit

var board = [Int]()

for (var x = 1; x <= 100; x++){
    board.insert(x, atIndex:x-1)
}
board

var num = 0
println("Game starts!\n")
println("You are at \(num)")
for i in board {
    if num < i {
        var dice = Int(arc4random_uniform(6)+1)
        num += dice
        println("Move \(dice) steps forward.")
        println("You are at \(num).")
        if (num > 100) {
            
        }
        if (num==10 || num==20 || num==30 || num==40 || num==50 || num==60 || num==70 || num==80 || num==90) {
            num = num - 4
            println("Move 4 steps backward.")
            println("You are at \(num)")
        }
        if (num==5 || num==15 || num==25 || num==35 || num==45 || num==55 || num==65 || num==75 || num==85 || num==95){
            num = num + 3
            println("Move 3 steps forward.")
            println("You are at \(num)")
        }
    }
}
